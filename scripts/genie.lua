--------------------------------------------------------------------------------
--  
-- TypeMAaster - Main project configuration file for GENIE 
--
-- Copyright (C) 2016 Patrick Rećko. All Rights Reserved.
-- The original author of this code can be contacted at: guest_none@protonmail.com

-- References: 
-- https://github.com/bkaradzic/bx/blob/master/scripts/genie.lua
-- https://github.com/bkaradzic/bgfx/blob/master/scripts/genie.lua
-- https://onedrive.live.com/view.aspx?cid=171ee76e679935c8&page=view&
-- resid=171EE76E679935C8!139573&parId=171EE76E679935C8!18835&authkey=
-- !AKv_SGrgJwxDGDg&app=PowerPoint
--
--


-- base set-up stuff
solution "typemaster"
   configurations {
			"Release",
			"Debug",
   }
	
   platforms {
         "x32",
         "x64",
		   "Native", -- for targets where bitness is not specified
   }
	
	language "C"
	

MAIN_DIR = path.getabsolute("..")
SOURCE_DIR = path.getabsolute("../src")
local BUILD_DIR = path.join(MAIN_DIR, "build")
local EXTERNAL_DIR = path.join(MAIN_DIR, "ext")

dofile ("toolchain.lua")
toolchain (BUILD_DIR, EXTERNAL_DIR)

function copyLib()
end

startproject "main"

project ("main")
	kind "ConsoleApp"
	
	includedirs {
		path.join(SOURCE_DIR, "**.h"),
	}
	
	files {
		path.join(SOURCE_DIR, "**.c"),
		path.join(SOURCE_DIR, "**.h"),
	}

