TypeMaster
==========

INTRO:
------
This is TypeMaster - a program which checks how great (or not)
you type on keyboard. You choose how much words do you want to
type and the program checks if you wrote them properly. Your results
can be saved to disk as a proof how great a keyboard rock star you are.

HOW TO USE
---------
First we're going to ask you to type your name. Then you will be send to
main menu. From which you can choose to start a session, display the results
of all session or save them.

ABOUT:
------
Written by Patrick Recko on January 2017 as a university project.
Released under MirOS License. Please check LICENSE.md for more informations.