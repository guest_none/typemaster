@echo off
echo Creating release folder.
IF EXIST rel rmdir rel /S /Q
mkdir rel
cd rel
copy ..\License.md .\
copy ..\Readme.md .\
copy ..\bin\Release\typemaster.exe .\
mkdir data
cd data
copy ..\..\data\dict.txt .\
pause
