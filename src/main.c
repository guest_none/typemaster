/** @file
 * Main program routine
 */
/*
 * Copyright (C) 2017 Patrick Rećko, All Rights Reserved.
 * Created by Patrick Rećko for project TypeMaster on 01/01/2017
 * The original author of this code can be contacted at: guest_none@protonmail.com
 *******************************************************************************
 * Licensed under MirOS license - it's text provided below
 * Provided that these terms and disclaimer and all copyright notices
 * are retained or reproduced in an accompanying document, permission
 * is granted to deal in this work without restriction, including un-
 * limited rights to use, publicly perform, distribute, sell, modify,
 * merge, give away, or sublicence.
 *
 * This work is provided "AS IS" and WITHOUT WARRANTY of any kind, to
 * the utmost extent permitted by applicable law, neither express nor
 * implied; without malicious intent or gross negligence. In no event
 * may a licensor, author or contributor be held liable for indirect,
 * direct, other damage, loss, or other issues arising in any way out
 * of dealing in the work, even if advised of the possibility of such
 * damage or existence of a defect, except proven that it results out
 * of said person's immediate fault when using the work as intended.
 */

/*---Header Files-------------------------------------------------------------*/
#include "pch.h"
#include "tm.h"

int
main()
{
	FILE *dict;
   struct UserInfo user;
   pr_lnklst_address_t word_bank;
   word_bank = NULL;
   int word_count, choice=1;
   bool saved=true;
   resetUserInfo(&user);

   enterHelpPart();
   clearScreen();

   getUserName(user.user_name);
   #if defined(_WIN32)
   dict = fopen("data\\dict.txt", "rt");
   #else
   dict = fopen("data/smalldict.txt", "rt");
   #endif
   if (dict == NULL) {
      printf("ERROR: Can't load dictionary file containing words\n");
      printf("Please, put the dict.txt file inside of data folder \n");
      printf("alongside the main executable\n");
      doPressAnyKey();
      return -1;
   }

   word_count = populateLinkedList(&word_bank, dict);

   #if defined(DEBUG)
   printf("%d\n", word_count);
   printLinkedList(word_bank);
   getCurrentTime(&user.run_date);
   printf("\nnow: %d-%d-%d %d:%d:%d\n",
          user.run_date->tm_year+1900,
          user.run_date->tm_mon +1,
          user.run_date->tm_mday,
          user.run_date->tm_hour,
          user.run_date->tm_min,
          user.run_date->tm_sec);
   doPressAnyKey();
   #endif // defined

   while(choice!=0) {
      choice = enterMainMenu();
      switch(choice) {
      case 1:
         saved=false;
         enterTypePart(word_bank, &user, word_count);
      case 2:
         enterResultsPart(user);
         break;
      case 3:
         saveSessionResults(user);
         saved = true;
         break;
      case 9:
         enterHelpPart();
         break;
      case 0:
         break;
      default:
         printf("Improper choice, please try again.");
         doSleep(2);
         break;
      }
   }
   if (saved == false) {
      printf("You probably forgot to save your results\n");
      doSleep(1);
      saveSessionResults(user);
   }

   destroyLinkedList(&word_bank);
   fclose(dict);
   return 0;
}
