/** @file
 * Functions used by the program
 */
/*
 * Copyright (C) 2017 Patrick Rećko, All Rights Reserved.
 * Created by Patrick Rećko for project TypeMaster on 01/01/2017
 * The original author of this code can be contacted at: guest_none@protonmail.com
 *******************************************************************************
 * Licensed under MirOS license - it's text provided below
 * Provided that these terms and disclaimer and all copyright notices
 * are retained or reproduced in an accompanying document, permission
 * is granted to deal in this work without restriction, including un-
 * limited rights to use, publicly perform, distribute, sell, modify,
 * merge, give away, or sublicence.
 *
 * This work is provided "AS IS" and WITHOUT WARRANTY of any kind, to
 * the utmost extent permitted by applicable law, neither express nor
 * implied; without malicious intent or gross negligence. In no event
 * may a licensor, author or contributor be held liable for indirect,
 * direct, other damage, loss, or other issues arising in any way out
 * of dealing in the work, even if advised of the possibility of such
 * damage or existence of a defect, except proven that it results out
 * of said person's immediate fault when using the work as intended.
 */
#pragma once

#define MAX_STRING_SIZE 100
#define LINKED_LIST_END NULL

/**Our own bool type since ANSI C doesn't have it*/
typedef int bool;
#define true 1
#define false 0

/**
 * @brief This structure is responsible for storing a word and it's size
 */
struct Word {
   char word[MAX_STRING_SIZE];
   int size;
};
/**
 * @brief This structure stores the per run information about the user
 */
struct UserInfo {
   char user_name[MAX_STRING_SIZE];
   int number_of_typed_words;
   int num_of_mistyped_words;
   /*for properly typed words*/
   int num_of_proper_words;
   struct tm *run_date;
};
/**
 * @brief This Linked list holds all loaded words to the memory
 */
struct LinkedList {
   struct Word word;
   struct LinkedList *next_element;
};

typedef struct LinkedList pr_lnklst_words_t;
typedef pr_lnklst_words_t *pr_lnklst_address_t;

////////////////////////////////////////////////////////////////////////////////
///// CORE STUFF

/**
 * @brief This function is responsible for displaying the main menu and
 *        selecting the choice
 *
 * @return User's choice (as a number)
 */
int enterMainMenu(void);

/**
 * @brief This function is responsible for doing the main part of the program
 *        (aka check if the user typed the word correctly
 *
 * @param [in] *words Word bank
 * @param [in] user User informations
 * @param [in] num_of_words Number of all loaded to the bank words
 */
void enterTypePart(pr_lnklst_words_t *words, struct UserInfo *user, int words_loaded);

/**
 * @brief This function displays the results of the users session
 *
 * @param [in] user User information
 */
void enterResultsPart(struct UserInfo user);

/**
 * @brief This function is responsible for getting the user's name
 *
 * @param [in] user_name Where the name should be stored
 */
void getUserName(char user_name[]);

/**
 * @brief This function resets the user info, so that it would display
 *        0 instead of the trash
 *
 * @param [in] *user_info Pointer to the user information
 */
void resetUserInfo(struct UserInfo *user_info);

/**
 * @brief This function saves the run results to users disk
 *        Note that this appends new dater instead of overwriting previous data
 *
 * @param [in] user_info Structure with the user informations
 */
void saveSessionResults(struct UserInfo user_info);

/**
 * @brief This function displays the help
 */
void enterHelpPart(void);

////////////////////////////////////////////////////////////////////////////////
///// UTILITIES

/**
 * @brief This function populates the linked list with data defined
 *        within the Word structure (creates the word bank)
 *
 * @param [in] *lnklst Given linked list
 * @param [in] *dict_file Pointer to the file containing the word dictionary
 * @return Number of elements (words) loaded to the linked list
 */
int populateLinkedList(pr_lnklst_address_t *lnklst, FILE *dict_file);

/**
 * @brief This function destroys the whole linked list
 *
 * @param [in] lnklst Given linked list
 */
void destroyLinkedList(pr_lnklst_address_t *lnklst);

#if defined(DEBUG)

/**
 * @brief This debug function prints the elements of the linked list
 *
 * @param [in] *lnklst Given linked list
 */
void printLinkedList(pr_lnklst_address_t *lnklst);

#endif // defined

/**
 * @brief This function sets the pointer to a given number of element of linked list
 *
 * @param [in] *lnklst Given linked list
 * @param [in] position Position in the list
 * @param [in] max the upper limit of our list
 * @return Address to a given element.
 */
pr_lnklst_address_t setPointerToElement(pr_lnklst_address_t *lnklst, int position, int max);

/**
 * @brief This function sets the pointer of the linked list to
 *        the last element or the element before the last
 *
 * @param [in] *lnklst pointer to the linked list
 * @param [in] offset 0 - last element of the list
 *                      1 - element before the last
 * @return pointer to the element
 */
pr_lnklst_address_t setPointerToEnd(pr_lnklst_address_t *lnklst, int offset);

/**
 * @brief This function clears the terminal screen
 */
inline void clearScreen(void);

/**
 * @brief This function pauses the execution of the program for a
 *        given number of seconds
 *
 * @param [in] seconds Number of seconds in which the program pauses
 */
void doSleep(int seconds);

/**
 * @brief This function stops the execution of
 *        the program until the user presses any key
 */
inline void doPressAnyKey(void);
/**
 * @brief This function read the current time
 *        and it saves to time structure
 *
 * @param [in] **currtime Given structure, where the time should be saved
 */
void getCurrentTime(struct tm **currtime);
