/** @file
 * Implementation of tm.h
 */
/*
 * Copyright (C) 2017 Patrick Rećko, All Rights Reserved.
 * Created by Patrick Rećko for project TypeMaster on 01/01/2017
 * The original author of this code can be contacted at: guest_none@protonmail.com
 *******************************************************************************
 * Licensed under MirOS license - it's text provided below
 * Provided that these terms and disclaimer and all copyright notices
 * are retained or reproduced in an accompanying document, permission
 * is granted to deal in this work without restriction, including un-
 * limited rights to use, publicly perform, distribute, sell, modify,
 * merge, give away, or sublicence.
 *
 * This work is provided "AS IS" and WITHOUT WARRANTY of any kind, to
 * the utmost extent permitted by applicable law, neither express nor
 * implied; without malicious intent or gross negligence. In no event
 * may a licensor, author or contributor be held liable for indirect,
 * direct, other damage, loss, or other issues arising in any way out
 * of dealing in the work, even if advised of the possibility of such
 * damage or existence of a defect, except proven that it results out
 * of said person's immediate fault when using the work as intended.
 */
/*---Header Files-------------------------------------------------------------*/
#include "pch.h"
#include "tm.h"

////////////////////////////////////////////////////////////////////////////////
///// CORE STUFF

int
enterMainMenu(void)
{
   int choice;
   clearScreen();
   printf("Welcome to TypeMaster\n");
   printf("========================================\n\n");
   printf("What do you want to do? \n\n");
   printf("1. Begin session\n");
   printf("2. View results of run\n");
   printf("3. Save the information about your run\n");
   printf("9. Display help\n");
   printf("0. Exit the program\n");
   printf("Please select your choice (1/2/3/9/0): ");
   scanf("%d", &choice);
   return choice;
}

void
enterTypePart(pr_lnklst_words_t *words,
              struct UserInfo *user,
              int words_loaded)
{
   pr_lnklst_address_t current_word=NULL;
   char typed_word[MAX_STRING_SIZE], c;
   clock_t start, end;
   int typed_word_size=0, num_of_words=0, i, word_pos, j=0, num_of_corr_letters=0;
   float percent;

   fflush(stdin);
   srand(time(NULL));
   for (;;) {
      clearScreen();
      printf("How much words do you want to type?: ");
      scanf("%d",&num_of_words);
      if (num_of_words<=0) {
         printf("Zero or negative number typed - please try again");
         doSleep(3);
         clearScreen();
      } else {
         clearScreen();
         break;
      }
   }
   for (i = 0; i<num_of_words; i++) {
      typed_word_size = 0;
      fflush(stdin);
      clearScreen();
      word_pos = rand()%words_loaded;
      current_word = setPointerToElement(words, word_pos, words_loaded);

      printf("Word to type: %s\n", &current_word->word.word);
      doSleep(3);
      printf("Timer will start with first key press.\nStart typing when you're ready... \n");
      c=getch();
      start=clock();
      while(c!='\r') {
         typed_word[typed_word_size]=c;
         typed_word_size+=1;
         c=getch();
      }
      end=clock();

      typed_word[typed_word_size]='\0';
      printf("You wrote %s.\n", typed_word);
      printf("Took %.2f seconds\n",
            (double)(end-start)/CLOCKS_PER_SEC);

      int size_cmp = strlen(typed_word);
      /*
       * First we check if typed word has the right length
       * then if the word has a proper size we check every letter.
       */
      if (size_cmp == current_word->word.size) {
         printf("Checking corectness of the word: \n");
         printf("%s\n", typed_word);
         num_of_corr_letters=0;
         while(j<current_word->word.size) {
            if (typed_word[j]==current_word->word.word[j]) {
               printf(" ");
               num_of_corr_letters+=1;
            } else {
               printf("~");
            }
            j+=1;
         }
         j=0;
         printf("\n");

         if (num_of_corr_letters == current_word->word.size) {
            printf("Typed correctly\n");
            user->number_of_typed_words+=1;
            user->num_of_proper_words+=1;
         } else {
            percent=(((float)1/size_cmp)*(size_cmp-num_of_corr_letters))*100;
            printf("Mistyped %.2f percent of letters\n", percent);
            user->number_of_typed_words+=1;
            user->num_of_mistyped_words+=1;
         }

      } else if (size_cmp < current_word->word.size){
         printf("The word that you typed is too short\n");
         user->number_of_typed_words+=1;
         user->num_of_mistyped_words+=1;
      } else {
         printf("The word that you typed is too long\n");
         user->number_of_typed_words+=1;
         user->num_of_mistyped_words+=1;
      }
      doPressAnyKey();
      memset(typed_word, 1, MAX_STRING_SIZE);
   }
}


void
enterResultsPart(struct UserInfo user)
{
   clearScreen();
   printf("User results\n");
   printf("========================================\n\n");
   printf("User name: %s \n", user.user_name);
   printf("Date of program run: %d-%d-%d %d:%d:%d\n",
          user.run_date->tm_year+1900,
          user.run_date->tm_mon +1,
          user.run_date->tm_mday,
          user.run_date->tm_hour,
          user.run_date->tm_min,
          user.run_date->tm_sec);
   printf("Number of typed words in this run: %d \n", user.number_of_typed_words);
   printf("Number of properly typed in this run: %d \n", user.num_of_proper_words);
   printf("Number of mistyped words in this run: %d \n\n", user.num_of_mistyped_words);

   doPressAnyKey();
}

void
getUserName(char user_name[])
{
   char choice;
   fflush(stdin);
   for (;;) {
      printf("What's your name?: ");
      gets(user_name);
      fflush(stdin);
      printf("Is %s OK? (y/n): ", user_name);
      scanf("%c", &choice);
      if ((choice=='y') || (choice=='Y'))
         break;
      else
         fflush(stdin);
   }
   clearScreen();


}

void
resetUserInfo(struct UserInfo *user_info)
{
   user_info->number_of_typed_words=0;
   user_info->num_of_mistyped_words=0;
   user_info->num_of_proper_words=0;
   getCurrentTime(&user_info->run_date);
}

void
saveSessionResults(struct UserInfo user_info)
{
   char choice;
   clearScreen();
   printf("Do you want to save your results? (y/n): ");
   scanf("%s", &choice);
   if ((choice=='y') || (choice=='Y')) {
      FILE *f;
      char buffer [1024];
      strcpy(buffer, user_info.user_name);
      strcat(buffer, "_results.txt");
      f=fopen(buffer, "at");
      if (f == NULL) {
         printf("ERROR ON SAVE");
         doSleep(3);
         return;
      }
      fprintf(f,"======================================================================\n");
      fprintf(f, "Results for user %s. Program ran on %d-%d-%d %d:%d:%d\n",
              user_info.user_name,
              user_info.run_date->tm_year+1900,
              user_info.run_date->tm_mon +1,
              user_info.run_date->tm_mday,
              user_info.run_date->tm_hour,
              user_info.run_date->tm_min,
              user_info.run_date->tm_sec);
      fprintf(f, "======================================================================\n");
      fprintf(f, "Number of typed words: %d \n", user_info.number_of_typed_words);
      fprintf(f, "Number of properly typed words: %d \n", user_info.num_of_proper_words);
      fprintf(f, "Number of mistyped words: %d \n", user_info.num_of_mistyped_words);
      fprintf(f, "======================================================================\n\n");
      fclose(f);
   }
}

void
enterHelpPart(void)
{
   clearScreen();
   printf("INTRO:\n");
   printf("This is TypeMaster - a program which checks how great (or not)\n");
   printf("you type on keyboard. You choose how much words do you want to \n");
   printf("type and the program checks if you wrote them properly. Your results\n");
   printf("can be saved to disk as a proof how great a keyboard rock star you are.\n");
   printf("\nHOW TO USE\n");
   printf("First we're going to ask you to type your name. Then you will be send to\n");
   printf("main menu. From which you can choose to start a session, display the results\n");
   printf("of all combined sessions or save them.\n");
   printf("\nABOUT:\n");
   printf("Written by Patrick Recko on January 2017 as a university project.\n");
   printf("Released under MirOS License. Please check LICENSE.md for more informations.\n\n");
   doPressAnyKey();
}
////////////////////////////////////////////////////////////////////////////////
///// UTILITIES

int
populateLinkedList(pr_lnklst_address_t *lnklst, FILE *dict_file)
{
   int word_count=0;
   char str[MAX_STRING_SIZE];
   #if defined(DEBUG)
      int strsize;
      pr_lnklst_address_t test = lnklst;
   #endif // defined
   pr_lnklst_address_t next=NULL;
   if (*lnklst!=NULL) {
      #if defined(DEBUG)
      printf("ERROR: this function should be called when the list is empty\n");
      #endif
      return -1;
   }

   while (fscanf(dict_file, "%s", str)==1) {
      if (*lnklst == NULL) {
         *lnklst = (pr_lnklst_address_t)malloc(sizeof(pr_lnklst_words_t));
         next = *lnklst;
      } else {
         next->next_element = (pr_lnklst_address_t)malloc(sizeof(pr_lnklst_words_t));
         next=next->next_element;
      }
      strcpy(next->word.word, str);
      next->word.size = strlen(next->word.word);
      #if defined(DEBUG)
      strsize=next->word.size;
      #endif // defined
      word_count+=1;
      next->next_element=NULL;
   }

   return word_count;
}

void
destroyLinkedList(pr_lnklst_address_t *lnklst)
{
	pr_lnklst_address_t helper, curr;

	if (*lnklst==NULL) {
		printf("ERROR: Linked List is empty");
		return;
	}
   helper = *lnklst;
	while (*lnklst != NULL) {
      if (helper->next_element != NULL) {
         curr = setPointerToEnd(&helper, 1);
         free(curr->next_element);
         curr->next_element = NULL;
      } else {
         free(*lnklst);
         *lnklst = NULL;
      }

	}

}

#if defined(DEBUG)
void
printLinkedList(pr_lnklst_address_t *lnklst)
{
   pr_lnklst_address_t curr;
   if (lnklst==NULL) {
      printf("This list is empty\n");
      return;
   } else {
      curr = lnklst;
      while(curr->next_element!=NULL) {
         printf("Word %s, sized %d",
                curr->word.word,
                curr->word.size);
         curr = curr->next_element;
      }
   }
}
#endif // defined

pr_lnklst_address_t
setPointerToElement(pr_lnklst_address_t *lnklst, int position, int max)
{
   int curr_num = 1;
   pr_lnklst_address_t curr_pos = NULL;

   if (*lnklst==NULL) {
      #if defined(DEBUG)
      printf("ERROR: The linked list is empty\n");
      #endif
      return NULL;
   }

   curr_pos = lnklst;

   while (curr_num!=position) {
      curr_pos = curr_pos->next_element;
      curr_num+=1;
   }
   return curr_pos;
}

pr_lnklst_address_t
setPointerToEnd(pr_lnklst_address_t *lnklst, int offset)
{
   if (*lnklst==NULL) {
      #if defined(DEBUG)
      printf("ERROR: The linked list is empty\n");
      #endif
      return NULL;
   }
   pr_lnklst_address_t helper = *lnklst, result=NULL;
   switch (offset) {
   case 0:
      for (;;) {
         if (helper->next_element == NULL) {
            result = helper;
            break;
         } else {
            helper = helper->next_element;
         }
      }
      break;
   case 1:
      for (;;) {
         if (helper->next_element->next_element == NULL) {
            result = helper;
            break;
         } else {
            helper = helper->next_element;
         }
      }
      break;
   }
   return result;
}

/*
 * TODO: This is a nasty hack that uses the clear commands on console
 * We should use termcap or something else
 */
inline void
clearScreen(void)
{
   // except cmd.exe most of the unix shells uses the clear command
   #ifdef _WIN32
      system("cls");
   #else
      system("clear");
   #endif
}

void
doSleep(int seconds)
{
   #ifdef _WIN32
      return Sleep(1000*seconds);
   #else
      return sleep(seconds);
   #endif
}

inline void
doPressAnyKey(void)
{
   printf("Press any key to continue...\n");
   getch();
}

void
getCurrentTime(struct tm **currtime)
{
   time_t rawtime;
   time(&rawtime);
   *currtime = localtime(&rawtime);
}
