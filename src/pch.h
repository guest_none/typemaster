/** @file
 * PreCompiled Header
 */
/*
 * Copyright (C) 2017 Patrick Rećko, All Rights Reserved.
 * Created by Patrick Rećko for project TypeMaster on 01/01/2017
 * The original author of this code can be contacted at: guest_none@protonmail.com
 *******************************************************************************
 * Licensed under MirOS license - it's text provided below
 * Provided that these terms and disclaimer and all copyright notices
 * are retained or reproduced in an accompanying document, permission
 * is granted to deal in this work without restriction, including un-
 * limited rights to use, publicly perform, distribute, sell, modify,
 * merge, give away, or sublicence.
 *
 * This work is provided "AS IS" and WITHOUT WARRANTY of any kind, to
 * the utmost extent permitted by applicable law, neither express nor
 * implied; without malicious intent or gross negligence. In no event
 * may a licensor, author or contributor be held liable for indirect,
 * direct, other damage, loss, or other issues arising in any way out
 * of dealing in the work, even if advised of the possibility of such
 * damage or existence of a defect, except proven that it results out
 * of said person's immediate fault when using the work as intended.
 */

/*---Header Files-------------------------------------------------------------*/
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifdef _WIN32
   #include <Windows.h>
#else
   #include <unistd.h>
#endif


#define MAX_STRING_SIZE 100

/*
 * if uncommented it displays some debug stuff related to linked list and date time saving
 * please don't uncomment
 */
//#define DEBUG
