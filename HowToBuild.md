How To build
------------
This program uses Code::Blocks - it's latest version (currently is 16.01) is required.
The version with built in GCC compiler is recommended.

To build:
	1. Load the typemaster.cbp project into Code::Blocks.
	2. 2et the build configuration into Release mode.
	3. Press the build button/shortcut
	4. Run via IDE to check if it's working.

The makerel.bat script would create the "rel" folder containing the
application compiled in release mode and it's data.