TypeMaster - mistrz pisania na klawiaturze
==========================================
| Patryk Rećko - grupa 7 - Podstawy programowania
| Politechnika Białostocka - semestr zimowy 2016/2017

Wstęp
-----
Ten program implementuje projekt numer 1. - "mistrz pisania na klawiaturze" 
z przedmiotu "Podstawy programowania" prowadzonym w semestrze zimowym 2016/2017.

Opis działania programu
-----------------------
##### Terminologia
   W programie użyto dwie nazwy. Sesja (session) dla określenia części programu, gdzie użytkownik trenuje swe umiejętności pisania, a przebieg (run)
   dla określenia zbioru sesji od uruchomienia do zakończenioa programu.
   
##### Głowna pętla
   1. Poproś użytkownika o podanie imienia z potwierdzeniem.
   2. Załaduj plik z bazą słow (jeśli nie istnieje poproś o jego skopiowanie do folderu data, który powinień się znajdować obok programu).
   3. Wyświetl głowne menu i poproś o wybór opcji.
      Jeśli wybierze 1, to rozpoczyna sesję. Jeśli 2, to wyświetl informacje o przebiegu. Numer 3 pyta o zapisanie
      informacje o przebiegu. 0 wychodzi z pętli wyboru.
   4. Jeśli użytkownik chociaż raz wykonał sesję, to jako zabezpieczenie sprawdzamy, czy użytkownik zapisał informacje o przebiegu, jeśli nie to pytamy się czy zapisać.
   5. Zniszcz naszą bazę słow, zamknij plik i zakończ program.

##### Sesja
   
   1. Poproś użytkownika, ile chce napisać słów.
   2. Wylosuj słowo z naszego banku słow.
   3. Użytkownik pisze słowo, nie widząc co pisze.
   4. Wyświetl napisane słowo.
   5. Sprawdż, czy słowo napisane ma taką samą długość, co słowo podane. Jesli nie to poinformuj użytkownika o tym, że słowo jest albo za krótkie albo za długie i wróć z powrotem do kroku 2.
   6. Sprawdż każdą literę słowa. Jeśli litera wpisana nie jest zgodna z literą wpisaną to ją podkreśl i zwiększ licznik błędnych liter.
   7. Poinformuj użytkownika, czy dobrze wpisał słowo (wyświetl procentowo liczbę błędnych liter jeśli są).
   8. Powrót do kroku 2.
   
   Algorytm sesji jest powtarzany od kroku 2. tyle razy ile wybrał słow do wpisania użytkownik. Na końcu sesji wyświetla się rezultat przebiegu.
   Po sprawdzaniu zapisujemy (lub zwiększamy jeśli w jednym przebiego uruchomiono więcej sesji) liczbę wpisanych błędnie i bezbłędnie słow oraz ich wszystkich wpisanych w przebiegu.

Lista funkcji
-------------
**NOTA**: Wszystkie funkcje są zdokumentowane po angielsku w pliku "tm.h" w folderze "src". Poniżej jest ich polskie tłumaczenie.

Dla osób nie zaznajomionych z JavaDoc'iem: **@brief** jest opisem funkcji, **@param** - parametrów, a **@return** opisuje,
co zwracamy.

~~~~~~~~~~~~~{.h}

////////////////////////////////////////////////////////////////
///// GŁÓWNE RZECZY

/**
 * @brief Wyświetla głowne menu i odpowiada za wybór.
 *
 * @return Wybór użytkownika (jako numer).
 */
int enterMainMenu(void);

/**
 * @brief Wykonytuje cześć programu odpowiedzialną za wpiysywanie
 *        słów i ich sprawdzanie.
 *
 * @param [in] *words Bank słow.
 * @param [in] user Struktura z informacjami o użytkowniku.
 * @param [in] num_of_words Liczba załadowanych do banku słów.
 */
void enterTypePart(pr_lnklst_words_t *words,
                   struct UserInfo *user, 
                   int words_loaded);

/**
 * @brief Wyświetla rezultat przebiegu użytkownika.
 *
 * @param [in] user Struktura z informacjami o użytkowniku.
 */
void enterResultsPart(struct UserInfo user);

/**
 * @brief Pyta i zapisuje imię użytkownika.
 *
 * @param [in] user_name Gdzie ma być zapisane imie.
 */
void getUserName(char user_name[]);

/**
 * @brief Resetuje informacje o użytkowniku, żeby nie wyświetlało 
 *        śmiecia.
 *
 * @param [in] *user_info Wskażnik na strukturę z informacjami 
 *                        o użytkowniku.
 */
void resetUserInfo(struct UserInfo *user_info);

/**
 * @brief Zapisuje dane o przebiegu na dysk w postaci 
 *        <imię użytkownika>_results.txt. Warto zaznaczyć, 
 *        że to dopisuje do pilku a nie nadpisuje.
 *
 * @param [in] user_info Struktura z informacjami o użytkowniku.
 */
void saveSessionResults(struct UserInfo user_info);

/**
 * @brief Wyświetla pomoc.
 */
void enterHelpPart(void);

////////////////////////////////////////////////////////////////
///// NARZĘDZIA

/**
 * @brief Wypełnia linked listę danymi zdefinowanymi w strukturze 
 *        Word (Wypełnia bank słów).
 *
 * @param [in] *lnklst Linked lista.
 * @param [in] *dict_file Wskażnik na plik z listą słow.
 * @return Ile załadowaliśmy elementów do listy.
 */
int populateLinkedList(pr_lnklst_address_t *lnklst, FILE *dict_file);

/**
 * @brief Niszczy całą listę.
 *
 * @param [in] lnklst Linked lista.
 */
void destroyLinkedList(pr_lnklst_address_t *lnklst);

#if defined(DEBUG)

/**
 * @brief Ta funkcja debugerska wyświetla cała linked listę.
 *
 * @param [in] *lnklst Linked lista.
 */
void printLinkedList(pr_lnklst_address_t *lnklst);

#endif // defined

/**
 * @brief Ustawia wskażnik na wybrany numerycznie element listy
 *
 * @param [in] *lnklst Linked lista.
 * @param [in] position Pozycja elementu.
 * @param [in] max Limit górny.
 * @return Wskażnik na element.
 */
pr_lnklst_address_t setPointerToElement(pr_lnklst_address_t *lnklst,
                                        int position,
                                        int max);

/**
 * @brief Ustawia wskażnik na ostatni lub przedostatni element listy.
 *
 * @param [in] *lnklst Linked lista.
 * @param [in] offset 0 - ostatni element listy
 *                    1 - element przed ostatnim
 * @return Wskażnik na element.
 */
pr_lnklst_address_t setPointerToEnd(pr_lnklst_address_t *lnklst, 
                                    int offset);

/**
 * @brief Czyści ekran konsoli (terminalu).
 */
inline void clearScreen(void);

/**
 * @brief Zatrzymuje wykonywanie programu na wybraną ilość sekund.
 *
 * @param [in] seconds Podana liczba sekund, gdzie program ma
 *                     być wstrzymany.
 */
void doSleep(int seconds);

/**
 * @brief Zatrzymuje wykonywanie programu do momentu naciśnięcia przycisku.
 */
inline void doPressAnyKey(void);

/**
 * @brief Ta funkcja zapisuje datę i czas w momencie wywołania funkcji.
 *
 * @param [in] **currtime Struktura, gdzie data ma być zapisane.
 */
void getCurrentTime(struct tm **currtime);

~~~~~~~~~~~~~

Kompilacja
----------
Wymagany jest **NAJNOWSZY STABILNY** Code::Blocks (w dniu pisania dokumentu jest to wersja 16.01) z dołączonym kompilatorem GCC. Jest on dostępny ze strony projektu.

Kompilacja jest prosta - załaduj projekt "typemaster.cbp" do Code::Blocks, ustaw typ kompilacji na Release, skompiluj i uruchom program z IDE w celu sprawdzenia jego działania.

Skrypt makerel.bat kopiuje wersję Release programu wraz z bankiem do folderu "rel". Tam można wtedy uruchomić program uruchamiając plik **typemaster.exe**